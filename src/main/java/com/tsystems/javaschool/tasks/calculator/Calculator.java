package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class Calculator {
    public static List<String> operators = Arrays.asList("+", "-", "/", "*");
    private Deque<String> stackForOperators;
    private Deque<String> resultList;

    public Deque<String> getStackForOperators() {
        return stackForOperators;
    }

    public void setStackForOperators(Deque<String> stackForOperators) {
        this.stackForOperators = stackForOperators;
    }

    public Deque<String> getResultList() {
        return resultList;
    }

    public void setResultList(Deque<String> resultList) {
        this.resultList = resultList;
    }

    public Calculator() {
        stackForOperators = new ArrayDeque<String>();
        resultList = new ArrayDeque<String>();
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == null || statement.isEmpty())
            return null;                                        //решение через запись в обратную польскую нотацию
        int paranthesesIsCorrect = 0;                                                  // инкремент на каждую открытую скобку и декремент на каждую закрытую
        statement = statement.replaceAll(" ", "");
        String prev = "";
        String current = "";
        StringTokenizer tokenizer = new StringTokenizer(statement, "*/+-()", true);
        while (tokenizer.hasMoreTokens()) {
            current = tokenizer.nextToken();
            if (isDigit(current)) {                                                     //проверка на операнд
                resultList.add(current);
                prev = current;
            } else if (isOperator(current)) {                                           // проверка на оператор
                if (isOperator(prev))
                    return null;                                      //2 оператора подряд без скобок - ошибка
                else if (prev.isEmpty() || prev.equals("(")) {                          //проверка на унарный минус, если да, то логика (-1)*
                    stackForOperators.push("*");
                    resultList.add("-1");
                    prev = current;
                } else {
                    if (!isPriority(current) || stackForOperators.peekFirst().equals("(")) {                  //проверка приоритетности операторов или наличие скобок в стеке
                        stackForOperators.push(current);
                        prev = current;
                    } else {
                        resultList.add(stackForOperators.pollFirst());
                        stackForOperators.push(current);
                        prev = current;
                    }
                }
            } else if (isParentheses(current)) {                                                              // работа со скобками
                paranthesesIsCorrect = current.equals("(") ? ++paranthesesIsCorrect : --paranthesesIsCorrect;
                if (paranthesesIsCorrect < 0) return null;
                if (current.equals("(")) {
                    stackForOperators.push(current);
                    prev = current;
                } else {
                    String temp = "";

                    while (!(temp = stackForOperators.pollFirst()).equals("(")) {
                        resultList.add(temp);
                    }
                }

            } else return null;

        }

        while ((stackForOperators.peekFirst()) != null) {
            resultList.add(stackForOperators.pollFirst());
        }

        if (paranthesesIsCorrect > 0) return null;                             //проверка на не закрытую скобку


        return calculationResult();
    }

    private boolean isParentheses(String current) {
        return (current.equals("(") || current.equals(")"));
    }


    public boolean isOperator(String s) {
        return operators.contains(s);
    }

    public boolean isDigit(String s) {
        return s.matches("([0-9]+)|([0-9]+\\.[0-9]+)") ? true : false;
    }

    public boolean isPriority(String s) {
        if (stackForOperators.isEmpty()) return false;
        boolean priorityOpInStack = (stackForOperators.peekFirst().equals("*") || stackForOperators.peekFirst().equals("/")) ? true : false; //  true - высокий приоритет, false - низкий приоритет
        boolean prorityOpInExpression = (s.equals("*") || s.equals("/")) ? true : false;
        if (!priorityOpInStack && prorityOpInExpression) return false;
        else return true;
    }

    public String calculationResult() {
        if (!resultList.stream().anyMatch(s -> operators.contains(s)) || resultList.size() < 3) return null;
        while (resultList.size() > 1) {
            String element = resultList.pollFirst();
            if (isOperator(element)) {
                String operand2 = resultList.pollLast();
                String operand1 = resultList.pollLast();
                if (operand1 == null || operand2 == null) return null;
                switch (element) {
                    case "+":
                        if (isDouble(operand1) || isDouble(operand2))
                            resultList.addLast(String.valueOf(Double.parseDouble(operand1) + Double.parseDouble(operand2)));
                        else
                            resultList.addLast(String.valueOf(Integer.parseInt(operand1) + Integer.parseInt(operand2)));
                        break;
                    case "-":
                        if (isDouble(operand1) || isDouble(operand2))
                            resultList.addLast(String.valueOf(Double.parseDouble(operand1) - Double.parseDouble(operand2)));
                        else
                            resultList.addLast(String.valueOf(Integer.parseInt(operand1) - Integer.parseInt(operand2)));
                        break;
                    case "*":
                        if (isDouble(operand1) || isDouble(operand2))
                            resultList.addLast(String.valueOf(Double.parseDouble(operand1) * Double.parseDouble(operand2)));
                        else
                            resultList.addLast(String.valueOf(Integer.parseInt(operand1) * Integer.parseInt(operand2)));
                        break;
                    case "/":
                        if (operand2.equals("0")) return null;
                        double result = 0;
                        result = Double.parseDouble(operand1) / Double.parseDouble(operand2);
                        if ((result - ((int) result) == 0)) resultList.addLast(String.valueOf((int) (result)));
                        else resultList.addLast(String.valueOf(result));

                        break;
                }


            } else
                resultList.addLast(element);
        }
        String result = resultList.getFirst();
        result = isDouble(result) ? String.valueOf(round(Double.parseDouble(result), 4)) : result;

        return resultList.peekFirst();
    }

    private boolean isDouble(String operand) {
        return operand.contains(".");
    }


    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
