package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {
    private int globalIndex =0;

    public int getGlobalIndex() {
        return globalIndex;
    }

    public void setGlobalIndex(int globalIndex) {
        this.globalIndex = globalIndex;
    }

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if(x==null || y==null) throw new IllegalArgumentException();
        if(x.size()>y.size() ) {
            return false;
        }

        if(x.isEmpty()) return true;

        for (Object object : y) {

            if (object == x.get(globalIndex)) {
                globalIndex++;
                if (globalIndex == x.size()) break;
            }
        }
        return globalIndex == x.size() ? true : false;

    }
}
