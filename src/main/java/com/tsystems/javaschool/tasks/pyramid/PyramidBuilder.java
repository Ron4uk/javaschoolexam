package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.List;

public class PyramidBuilder {
    private int baseLength = 0;

    public int getCount() {
        return baseLength;
    }

    public void setCount(int count) {
        this.baseLength = count;
    }


    public boolean isCanBuild(List<Integer> list) {                             // проверка на  возможность создания пирамиды
        if (list.contains(null) || list.size() < 3) return false;               // вычитаем из элементов (длина List'а) количесто необходимое на каждую из строк пирамид
        double d = (-1+Math.sqrt(1+4*2*list.size()))/2;                          // расчет через арифметическую прогрессию
        if(d==(int)d){
            baseLength=(int)d;
            return true;
        }
        else return false;
    }

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (isCanBuild(inputNumbers)) {
            Collections.sort(inputNumbers);
            Deque<Integer> queue = new ArrayDeque<>(inputNumbers);
            int indentFromEdge = 0;
            int[][] result = new int[baseLength][baseLength + baseLength - 1];

            for (int i = result.length - 1; i >= 0; i--) {
                for (int k = result[i].length - 1 - indentFromEdge; k >= 0 + indentFromEdge; k--) {
                    if (k == result[i].length - 1 - indentFromEdge) result[i][k] = queue.pollLast();
                    else if (result[i][k + 1] != 0) result[i][k] = 0;
                    else result[i][k] = queue.pollLast();
                }
                indentFromEdge++;
            }

            return result;
        } else throw new CannotBuildPyramidException();
    }


}
